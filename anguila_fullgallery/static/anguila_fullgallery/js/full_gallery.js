django.jQuery(document).ready(function()
{
	full_gallery.init();
})

var full_gallery = {
	current_page:1,
	init:function()
	{
		full_gallery.enableSearch();
		full_gallery.search("");
		
		django.jQuery("#update_data_image_btn").click(function()
		{

			var image_id = django.jQuery("#image_id_edit").val() 
			var csrfmiddlewaretoken =django.jQuery("input[name='csrfmiddlewaretoken']").val()
			var title = django.jQuery("#edit_window input[name='title_edit']").val();
			var description = django.jQuery("#edit_window textarea[name='description_edit']").val();
			var d = {id:image_id,
					csrfmiddlewaretoken:csrfmiddlewaretoken,
					title:title,
					description:description}
			console.log(d)
			django.jQuery.post(full_gallery.getEditPath(),d,function(response)
			{
				console.log("Actualizado")

			})

			return false;
		})

		django.jQuery("#close_edit_window").click(function()
		{
			django.jQuery("#edit_window").css("visibility","hidden")
			django.jQuery("#edit_window input[name='title_edit']").val("");
			django.jQuery("#edit_window textarea[name='description_edit']").val("");
			return false;
		})
	},
	getCurrentCategory:function()
	{
		var path = window.location.pathname.split("/")
		var uri = "/"+path[1]+"/"+path[2]+"/search/"
		var gid = path[3]
		return gid
	},
	getEditPath:function()
	{
		var path = window.location.pathname.split("/")
		var uri = "/"+path[1]+"/"+path[2]+"/edit_image/"
		return uri
	},
	getDeletePath:function()
	{
		var path = window.location.pathname.split("/")
		var uri = "/"+path[1]+"/"+path[2]+"/delete_image/"
		return uri
	},
	getSearchPath:function()
	{
		var path = window.location.pathname.split("/")
		var uri = "/"+path[1]+"/"+path[2]+"/search/"

		return uri
	},
	enableEdition:function()
	{
		django.jQuery(".image_element_links_container #delete_image_btn").click(function()
		{
			var csrfmiddlewaretoken =django.jQuery("input[name='csrfmiddlewaretoken']").val()
			var image_id = django.jQuery(this).parent().parent().find("img").eq(0).attr("rel")
			var obj_to_delete = django.jQuery(this).parent().parent();
			django.jQuery.post(full_gallery.getDeletePath(),{id:image_id,csrfmiddlewaretoken:csrfmiddlewaretoken,gid:full_gallery.getCurrentCategory()},function(response)
			{
				full_gallery.search("",full_gallery.current_page);
			})
			return false;
		})
		django.jQuery(".image_element_links_container #edit_image_btn").click(function()
		{

			var image_id = django.jQuery(this).parent().parent().find("img").eq(0).attr("rel")
			var csrfmiddlewaretoken =django.jQuery("input[name='csrfmiddlewaretoken']").val()
			var t = django.jQuery(this).parent().parent().offset().top + 10;
			var l = django.jQuery(this).parent().parent().offset().left + 110


			if(l+200 > django.jQuery("#images_container").innerWidth())
			{
				l = l-348;
			}
			django.jQuery("#edit_window").css("top",t);
			django.jQuery("#edit_window").css("left",l);

			django.jQuery.post(full_gallery.getEditPath(),{id:image_id,get_data:true,csrfmiddlewaretoken:csrfmiddlewaretoken},function(response)
			{
				response = django.jQuery.parseJSON(response)
				django.jQuery("#image_id_edit").attr("value",image_id);
				django.jQuery("#edit_window").css("visibility","visible");

				django.jQuery("#edit_window input[name='title_edit']").val(response[0].fields.title);
				django.jQuery("#edit_window textarea[name='description_edit']").val(response[0].fields.description);
			})

			return false;
		})
	},
	enablePagination:function()
	{
		django.jQuery("#paginator_container").find("a").click(function()
		{
			full_gallery.search("",django.jQuery(this).text());
			return false;
		})
	},
	enableSearch:function()
	{
		django.jQuery("#search_box").keyup(function()
		{
					
			full_gallery.search(django.jQuery(this).val());

		})
	},
	search:function(w,page)
	{
		var csrfmiddlewaretoken =django.jQuery("input[name='csrfmiddlewaretoken']").val()
		d = {gid:full_gallery.getCurrentCategory(),w:w,csrfmiddlewaretoken:csrfmiddlewaretoken}
		if(page !=null)
		{
			d.page = page	
		}
		django.jQuery("#images_controls").find("img").css("visibility","visible");

		var new_element = '<div class="image_element">'
		new_element += '<div class="image_element_links_container">'
		new_element += '<a id="edit_image_btn" style="position: relative;left: 10px;" href="#" ><div class="image_element_links"><img src="/static/admin/img/icon_changelink.gif"/></div></a>'
		new_element += '<a id="delete_image_btn" style="position: relative;left: 10px;" href="#" ><div class="image_element_links"><img src="/static/admin/img/icon_deletelink.gif"/></div></a>'
		new_element += '</div>'
		new_element += '</div>'
			

		django.jQuery.post(full_gallery.getSearchPath(),d,function(response)
		{
			django.jQuery(".image_element").remove();
			response = django.jQuery.parseJSON(response)
			for(var i = 0;i<response.data.length;i++)
			{
				var e = django.jQuery(new_element);
				var a = '<img src="'+response.data[i].fields.image_thumb+'" rel="'+response.data[i].pk+'"/>'
				e.prepend(a);
				django.jQuery("#images_container").append(e);
			}

			django.jQuery("#paginator_container > ul").find("li").remove()
			if(response.data.length >0)
			{
				for(var i = 1;i<parseInt(response.numpages)+1;i++)
				{
					django.jQuery("#paginator_container > ul").append("<li><a href='#'>"+i+"</a></li>");
				}	
			}

			full_gallery.current_page = parseInt(response.current_page)

			full_gallery.enablePagination();
			full_gallery.enableEdition();

			django.jQuery("#images_controls").find("img").css("visibility","hidden");

		})
	}
}