# -*- coding: utf-8 -*-
from django.db import models
from sorl.thumbnail import ImageField
from sorl.thumbnail import get_thumbnail
from django.utils.translation import ugettext_lazy as _


class Gallery(models.Model):

    title = models.CharField(_("Title"), max_length=200)
    description = models.TextField(_("Description"), max_length=200)
    image = ImageField(_("Image"), upload_to='gallery')

    def __str__(self):
        return self.title

    def __unicode__(self):
        return self.title

    def image_thumb(self):
        im = get_thumbnail(self.image, '100x100', crop='center', quality=99)
        return "<img src='" + im.url + "' />"
    image_thumb.allow_tags = True
    image_thumb.short_description = _("Image")


class GalleryImage(models.Model):

    gallery = models.ManyToManyField(Gallery)
    title = models.CharField(_("Title"), max_length=200)
    description = models.TextField(_("Description"), max_length=200)
    image = ImageField(_("Image"), upload_to='fullgallery_images')

    def __str__(self):
        return self.title

    def __unicode__(self):
        return self.title

    @property
    def image_thumb(self):
        im = get_thumbnail(self.image, '100x100', crop='center', quality=99)
        return im.url

    @property
    def image_thumb_200x200(self):
        im = get_thumbnail(self.image, '200x200', crop='center', quality=99)
        return im.url
